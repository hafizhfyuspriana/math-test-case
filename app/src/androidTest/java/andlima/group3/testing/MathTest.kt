package andlima.group3.testing

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import junit.framework.TestCase

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.math.RoundingMode
import java.text.DecimalFormat

@RunWith(AndroidJUnit4::class)
class MathTest : TestCase() {

    @Before
    public override fun setUp() {
        super.setUp()
    }

    @After
    public override fun tearDown() {}

    @Test
    fun testSum() {
        val msg = "[TEST] SUM"
        val expected = 5.57

        val test = getTwoDecimalPlace(Math().sum(1.22,4.35))
        Log.d(msg, test.toString())

        if (test != expected) {
            fail("$msg: Result inaccurate")
            failNotEquals(msg, expected, test)
        } else {
            assertEquals(msg, expected, test)
        }
    }

    @Test
    fun testSubtract() {
        val msg = "[TEST] SUBTRACT"
        val expected = 41.90

        val test = getTwoDecimalPlace(Math().subtract(53.20, 11.30))
        Log.d(msg, test.toString())

        if (test != expected) {
            fail("$msg: Result inaccurate")
            failNotEquals(msg, expected, test)
        } else {
            assertEquals(msg, expected, test)
        }
    }

    @Test
    fun testMultiply() {
        val msg = "[TEST] MULTIPLY"
        val expected = 369.00

        val test = getTwoDecimalPlace(Math().multiply(3.00, 123.00))
        Log.d(msg, test.toString())

        if (test != expected) {
            fail("$msg: Result inaccurate")
            failNotEquals(msg, expected, test)
        } else {
            assertEquals(msg, expected, test)
        }
    }

    @Test
    fun testDivision() {
        val msg = "[TEST] AVERAGE"
        val expected = 8.02

        val test = getTwoDecimalPlace(Math().division(96.24, 12.00))
        Log.d(msg, test.toString())

        if (test != expected) {
            fail("$msg: Result inaccurate")
            failNotEquals(msg, expected, test)
        } else {
            assertEquals(msg, expected, test)
        }
    }

    @Test
    fun testAverage() {
        val msg = "[TEST] AVERAGE"
        val expected = 33.35

        val test = Math().average(doubleArrayOf(11.15, 22.25, 33.35, 44.45, 55.55))
        Log.d(msg, test.toString())

        if (test != expected) {
            fail("$msg: Result inaccurate")
            failNotEquals(msg, expected, test)
        } else {
            assertEquals(msg, expected, test)
        }
    }

    private fun getTwoDecimalPlace(num: Double): Double {
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.HALF_UP

        return df.format(num).toDouble()
    }
}