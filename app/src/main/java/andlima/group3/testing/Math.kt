package andlima.group3.testing

class Math {
    fun sum(num1: Double, num2: Double) : Double {
        return num1 + num2
    }

    fun subtract(num1: Double, num2: Double) : Double {
        return num1 - num2
    }

    fun multiply(num1: Double, num2: Double) : Double {
        return num1 * num2
    }

    fun division(num1: Double, num2: Double) : Double {
        return num1 / num2
    }

    fun average(args: DoubleArray) : Double {
        var total = 0.0
        
        args.forEach { num ->
            total = sum(total, num)
        }
        
        return division(total, args.size.toDouble())
    }
}